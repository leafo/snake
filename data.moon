
export List
export GridCollide, ListHasher, Box
export Color

-- maybe use a skip list for z ordering?
class List
  new: =>
    @clear!

  clear: =>
    @front = { next: nil }
    @back = { prev: @front }
    @front.next = @back
    @items = {} -- item to nodes

  is_empty: =>
    @front.next == @back

  _node: (item) =>
    error "can't add nil to list" if item == nil
    error "node already in list" if @items[item]
    n = { value: item, prev: nil, next: nil }
    @items[item] = n
    n

  remove: (item) =>
    node = @items[item]
    return false if not node

    before = node.prev
    after = node.next
    before.next = after
    after.prev = before
    @items[item] = nil
    item

  -- after is a node, not an item
  _insert_after: (item, after) =>
    node = @_node item
    node.next = after.next
    node.prev = after
    after.next.prev = node
    after.next = node

  push_front: (item) =>
    @_insert_after item, @front

  push_back: (item) =>
    @_insert_after item, @back.prev

  pop_front: =>
    first = @front.next
    error "Don't have element to pop" if first.value == nil
    @remove first.value

  pop_back: =>
    last = @back.prev
    error "Don't have element to pop" if last.value == nil
    @remove first.value

  each: =>
    coroutine.wrap ->
      current = @front.next
      while current != @back
        coroutine.yield current.value
        current = current.next

  contains: (item) =>
    @items[item] != nil

  get_size: =>
    i = 0
    for _,_ in pairs @items
      i += 1
    i

class Color
  new: (@r, @g, @b) =>
  __tostring: => ("color<(%d, %d, %d)>")\format @r, @g, @b

class Box
  sized: (ax, ay, w, h) ->
    Box ax, ay, ax + w, ay + h

  new: (@ax, @ay, @bx, @by) =>

  contains_pt: (x, y) =>
    x > @ax and x < @bx and y > @ay and y < @by

  collides_with: (box) =>
    not (@ax > box.bx or box.ax > @bx or @ay > box.by or box.ay > @by)

  unpack_coords: =>
    @ax, @ay, @bx, @by

  unpack: =>
    @ax, @ay, @bx - @ax, @by - @ay

  __tostring: => ("box<(%d, %d), (%d, %d)>")\format @ax, @ay, @bx, @by

class ListHasher
  new: =>
    @root = {}

  hash: (list) =>
    current = @root
    for item in *list
      if not current[item]
        current[item] = {}
      current = current[item]
    current

hasher = ListHasher!

class GridCollide
  new: (@grid_size=100) =>
    @grid = defaultbl -> {}

  hash_point: (x, y) =>
    -- concat {x, ":", y} -- Whicha is fAsta
    hasher\hash {x,y}

  cell_for_pt: (x,y) =>
    xx, yy = x - x % @grid_size, y - y % @grid_size
    hash = @hash_point xx, yy
    @grid[hash]

  cells_for_box: (box) =>
    import ax, ay, bx, by from box
    coroutine.wrap ->
      while ay < by
        while ax < bx
          coroutine.yield @cell_for_pt ax, ay
          ax += @grid_size
        ay += @grid_size

  add: (item) =>
    for cell in @cells_for_box item.box
      insert cell, item

  find_in_box: (box) =>
    found = {}
    seen = {}
    for cell in @cells_for_box box
      for item in *cell
        if not seen[item]
          seen[item] = true
          if item.box\collides_with box
            insert found item

    found

  find_in_pt: (x, y) =>
    cell = @cell_for_pt x,y
    for item in *cell
      if item.box\contains_pt x,y
        return item
    nil

