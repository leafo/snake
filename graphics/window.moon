
module "graphics.window", package.seeall

require "support"
require "graphics.image"

import max from math

export Window

border = {
  tl: 0
  l: 1
  t: 2
  tr: 3
  bl: 4
  r: 5
  b: 6
  br: 7
  back: 8
}

class Window
  border_size: 4

  new: (ui_image, props) =>
    mixin_table self, props if props
    @parts = ui_image\sprited @border_size, @border_size, items_x: 4

  draw: (x, y, w, h) =>
    w = max w, @border_size*2
    h = max h, @border_size*2

    x2 = x + w - @border_size
    y2 = y + h - @border_size

    s_x = (w - @border_size*2) / @border_size
    s_y = (h - @border_size*2) / @border_size

    lr_ystart = y + @border_size
    tb_xstart =  x + @border_size

    with @parts
      \draw border.tl, x, y
      \draw border.tr, x2, y

      \draw border.bl, x, y2
      \draw border.br, x2, y2

      \draw border.l, x, lr_ystart, nil, 1, s_y
      \draw border.r, x2, lr_ystart, nil, 1, s_y

      \draw border.t, tb_xstart, y, nil, s_x, 1
      \draw border.b, tb_xstart, y2, nil, s_x, 1

      \draw border.back, tb_xstart, lr_ystart, nil, s_x, s_y


