
require "support"
module "image", package.seeall

export loader, load

-- knows how to blit from a uniform grid
class Spriter
  offset_x: 0
  offset_y: 0
  padding_x: 0
  padding_y: 0

  new: (@image, @cell_width, @cell_height) =>
    @items_x = math.floor @image\getWidth! / cell_width
    -- @items_y = math.floor @image\getHeight! / cell_height -- not needed
    @clear!

  clear: =>
    @quads = {}

  draw: (id, x, y, ...) =>
    if not @quads[id]
      sx = (id % @items_x) * (@cell_width + @padding_x)
      sy = (math.floor id / @items_x) * (@cell_height + @padding_y)

      -- quad = love.graphics.newQuad( x, y, width, height, sw, sh )
      iw, ih = @image\getWidth!, @image\getHeight!

      print sx, sy, iw, ih

      @quads[id] = love.graphics.newQuad sx, sy, @cell_width, @cell_width, iw, ih

    love.graphics.drawq @image.tex, @quads[id], x, y, ...

class Image
  new: (@tex) =>
    @tex\setFilter "nearest", "nearest" -- is this even working?
    print "Filter:", @tex\getFilter!
    mixin_object self, @tex, {"getWidth", "getHeight", "setFilter"}

  draw: (x, y) =>
    love.graphics.draw @tex, x, y

  sprited: (width, height, props) =>
    s = Spriter self, width, height
    if props
      for key, val in pairs props
        s[key] = val
    s

class ImageLoader
  new: (@dir) =>
    @loaded = {}

  _load: (name) =>
    love.graphics.newImage @dir..name

  load: (name) =>
    if not @loaded[name]
      @loaded[name] = Image @_load name
    @loaded[name]

  reload_all: =>
    for name, image in pairs @loaded
      image.tex = @_load name

loader = ImageLoader conf.image_dir
load = (...) -> loader\load ...
