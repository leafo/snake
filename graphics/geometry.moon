
require "support"

export Vec2

class Vec2
  new: (@x=0, @y=0) =>

  length: =>
    math.sqrt @x*@x + @y*@y

  unpack: => @x, @y

  normalized: =>
    len = @length!
    Vec2 @x / len, @y / len

  __tostring: => ("vec2<(%f, %f)>")\format @x, @y

  __mul: (other) =>
    if type(other) == "number"
      Vec2 @x * other, @y * other
    else -- dot product
      @x * other.x + @y + other.y

  __div: (other) =>
    Vec2 @x / other, @y / other

  __add: (other) =>
    Vec2 @x + other.x, @y + other.y

  __sub: (other) =>
    Vec2 @x - other.x, @y - other.y

