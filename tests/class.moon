
require "support"

class HelloWorld
  new: (@arg) =>
    @cool = 100

  hi: =>
    print "cool", @cool, "arg", @arg

class Poop
  new: =>
    mixin self, HelloWorld, "umm yeah"

p = Poop!
print p.cool
p\hi!
