
require "support"

l = List!

print i for i in l\each!

l\push_back "hello world"

print "*"
print i for i in l\each!


l\push_back "end me"
l\push_front "hi dad"

print "*"
print i for i in l\each!

l\remove "hello world"

print "*"
print i for i in l\each!


l\remove "end me"

print "*"
print i for i in l\each!


l\remove "hi dad"

print "*"
print i for i in l\each!

