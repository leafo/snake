
require "support"

tbl = defaultbl -> {}

tbl.hello.world = "something"
print tbl.COOL
print dump tbl

b1 = Box 10, 10, 30, 30
b2 = Box 20, 15, 40, 20
b3 = Box 15, 15, 25, 25
b4 = Box 40, 40, 41, 41

print "b1 b2:", b1\collides_with b2
print "b2 b1:", b2\collides_with b1
print "b1 b3:", b1\collides_with b3
print "b1 b4:", b1\collides_with b4

h = ListHasher!
print h\hash({1,2,3,4,5}) == h\hash({1,2,3,4,5})
print h\hash({"hello"}) == h\hash({"world"})

c = GridCollide(10)
-- print dump c\cell_for_pt 12, 120

b1.box = b1

c\add b1
print dump c
print c\find_in_pt 15, 15
print c\find_in_pt 31, 31

