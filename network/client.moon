
require "enet"
require "support"
require "ui.base" -- for console
shared = require "network.shared"

module "network.client", package.seeall
require "network.shared"

import Sequence from require "ui.sequence"

export ServerClient

class Client
  new: (@pid) =>

-- umm
class NetworkSequence
  new: (@server, fn) =>
    @messages = List!
    super fn, {
      get_msg: ->
        while @messages\is_empty
          coroutine.yield
        @messages\pop_front!
    }

  push_msg: (msg) =>
    @messages\push_back msg

class ServerClient
  connected_to_server: false

  new: (ip, port=3723) =>
    @host = enet.host_create!
    addr_string = ("%s:%s")\format ip, port
    @server = @host\connect addr_string

  update: (dt) =>
    while true
      event = @host\service 0
      break if not event

      if event.type == "connect"
        print "Connected to", event.peer

        ui.console\log "Connected to server: "..tostring(event.peer)
      elseif event.type == "receive"
        "cool"

-- hec
find_player_with_pid = (pid) ->
  for player in *all_players
    if player.pid == pid then return player

  return nil

load = ->
  server = host\connect network.ip..":"..network.port
  math.randomseed( os.time() )
  math.random()
  math.random()
  math.random()
  console = ui.current_console

handle_network_msgs = ->
  event = 1
  while event
    event = host\service 0 -- smallest possible timeout
    if event
      if event.type == "connect"
        connected_to_server = true
        print "Connected to", event.peer
        console\log "Connected to server: "..tostring(event.peer)
      elseif event.type == "receive"
        deserialized = network.deserialize event.data
        msg_type = deserialized[1]

        ---- CHAT MESSAGES ----
        if msg_type == network.type_chat
          console\log deserialized[2]..": "..deserialized[3]


        ---- WORLD MESSAGES ----
        elseif msg_type == network.type_your_player
          my_player = Player.deserialize deserialized[2]
          console\log "Received your player: "..my_player\serialize!
          table.insert all_players, my_player
        elseif msg_type == network.type_other_player
          other_player = Player.deserialize deserialized[2]
          console\log "Received other player: "..other_player\serialize!
          table.insert all_players, other_player
          
        elseif msg_type == network.type_populate_entity
          entity = Entity.deserialize deserialized[2], find_player_with_pid
          console\log "Received an entity: "..entity\serialize!
          all_entities[entity.eid] = entity
          
          -- my_player.hero will be set upon deserializing the entity
          -- if entity == my_player.hero
          --   scroll_to_hero!


        ---- MOVEMENT MESSAGES ----
        elseif msg_type == network.type_move_from_server
          eid = tonumber(deserialized[2])
          dir = tonumber(deserialized[3])
          entity_to_move = all_entities[eid]
          if entity_to_move
            --~ console\log "X,Y="..tostring(player_to_move.x)..','..tostring(player_to_move.y)..' dir='..tostring(dir)
            entity_to_move\move dir
          else
            console\log "[move] Couldn't find entity with eid = "..tostring(eid)

        elseif msg_type == network.type_set_position
          x = tonumber(deserialized[2])
          y = tonumber(deserialized[3])
          my_player.x = x
          my_player.y = y
          console\log "Server adjusted your position to X,Y="..tostring(my_player.x)..','..tostring(my_player.y)
          
        ---- DEBUG MESSAGES ----
        elseif msg_type == network.type_debug_click
          pid = tonumber(deserialized[2])
          x = tonumber(deserialized[3])
          y = tonumber(deserialized[4])
          make_part_system2 x,y,pid

        ---- DISCONNECT ----
        elseif msg_type == network.type_disconnect
          pid = tonumber(deserialized[2])
          player_to_disconnect = find_player_with_pid pid
          if player_to_disconnect
            console\log "Server: "..player_to_disconnect.name.." disconnected."
            remove_from_array all_players, player_to_disconnect
            
            -- Remove all of the entities
            for entity in *player_to_disconnect.entities
              all_entities[entity.eid] = nil
            
          else
            console\log "[disc] Couldn't find player with pid = "..tostring(pid)

        ---- UNKNOWN MESSAGE ----
        else
          err = "Couldn't understand message. "
          if not deserialized
            err..= "deserialized is nil."
          else
            err..="Type = "..tostring(msg_type)
          console\log err

        --~ console\log "Got message of type="..tostring(msg_type)
        --~ print("Got message: ", event.data, event.peer)

-- BRING BACK the heck balls
update = (dt) ->
  dead = {}
  for k,v in pairs psystems
    v[1]\update dt
    if not v[1]\isActive! and v[2] == -1
      v[1]\stop!
      v[2] = 100
    if v[2] != -1
      v[2] = v[2] - 1
      if v[2] == 0
        table.insert dead, k

  for d in *dead do remove_from_array psystems, d


