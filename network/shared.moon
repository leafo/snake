
require "support"
-- module "network.shared", package.seeall
-- no module, include after defining network package

export MessageHandler, log

names = {'Volmog', 'Balkhnarb', 'Lurgagal',
  'Oggar', 'Lurgubal', 'Balkhmog',
  'Tarkhubal', 'Bolnarb', 'Tarkhrod', 'Durbarod'}

random_name = ->
  names[math.random(1,#names)]

log = (msg) -> print "["..(os.date "%H:%M:%S").."] "..msg

class MessageHandler
  new: (@server, @actions) =>
  dispatch: (msg) =>
    handler = @actions[msg.type] or @actions[1]
    error "got unknown message type: " .. tostring msg.type if not handler
    handler @server, msg



-- old things
class Entity
  next_eid: 1
  -- Arguments:
  -- owner: a Player object. Cannot be nil. When serialized, only the pid is sent across.
  --    The owner that you pass in will automatically be given this entity.
  -- eid: entity ID. If nil, the next unassigned eid will be used.
  -- type: the type of entity (e.g. dragon, slime, hero, etc. Unused for now)
  new: (@name, @eid, @owner, @x, @y, @type ) =>
    if not @owner
      print "Warning! Created an entity with a nil owner!"
    else
      @owner\give_entity self
      
    if not @x then @x = math.random 1,700
    if not @y then @y = math.random 1,500
    if not @eid
      @eid = Entity.next_eid
      Entity.next_eid += 1
    if not @type
      @type = 'unspecified'
    if not @name
      @name = random_name!
      
    -- The direction they are facing
    @facing = direction.down
    
    if not @owner then @owner = {pid:'nopid'}

  serialize: =>
    return @name..' '..tostring(@eid)..' '..tostring(@owner.pid)..' '..tostring(@x)..' '..tostring(@y)..' '..tostring(@facing)

  -- pid_to_player_fn a function that turns a pid into a Player
  -- so that both client and server can deserialize in their own way.
  deserialize: (serialized, pid_to_player_fn) ->
    split = moonscript.util.split serialized, ' '
    name = split[1]
    eid = tonumber(split[2])
    
    owner = pid_to_player_fn tonumber(split[3])
    
    x,y = tonumber(split[4]), tonumber(split[5])
    facing = tonumber(split[6])
    
    entity = Entity name, eid, owner, x, y
    entity.facing = facing
    return entity
    
  update: =>


  move: (dir) =>
    if dir == direction.up then @y -= 5
    if dir == direction.right then @x += 5
    if dir == direction.down then @y += 5
    if dir == direction.left then @x -= 5
    
    @facing = dir
  
  draw_text_label: (text, quad, scroll_x, scroll_y) =>
    font_height = love.graphics.getFont!\getHeight!
    font_width = love.graphics.getFont!\getWidth @name
    qx,qy,qw,qh = quad\getViewport!
    x_offset = (font_width - qw) / 2
    
    love.graphics.setColor 255,0,0
    love.graphics.print text, @x - x_offset - scroll_x, @y - font_height - scroll_y
    
  draw: (image, scroll_x, scroll_y) =>
    -- Get a quad based on what direction we're facing
    quad = love.graphics.newQuad @facing * 49, 0, 48, 50, image\getWidth!, image\getHeight!
  
    @draw_text_label @name, quad, scroll_x, scroll_y
  
    love.graphics.setColor 255,255,255
    love.graphics.drawq image, quad, @x - scroll_x, @y - scroll_y

class Player
  next_pid: 1

  -- name is required
  -- peer is only used by the server
  new:(@name, @pid, @peer) =>
    if not @pid
      @pid = Player.next_pid
      Player.next_pid += 1
      
    -- Array of entities that this player owns. These are not serialized with the player.
    @entities = {}
    
    -- Your hero entity is always the first entity that you have
    @hero = nil
  
  give_entity: (entity) =>
    if #@entities == 0 then @hero = entity
    table.insert @entities, entity
    entity.owner = self
  
  remove_entity: (entity) =>
    print "[reme] Remove doesn't do anything yet"
    -- Make sure you can't remove your hero
  
  serialize: =>
    return @name..' '..tostring(@pid)

  deserialize: (serialized) ->
    split = moonscript.util.split serialized, ' '
    new_player= Player split[1], tonumber(split[2])
    return new_player

