require 'enet'
require "support"

module "network.client", package.seeall
require "network.shared"

usernames = {"Elrohir", "Feanaro", "Elros"}

-- message builders
message = {
  connect: (client) -> { type: "connect", pid: client.pid }

  create_player: (client) -> { type: "create_player", pid: client.pid }

  disconnect: (client) -> { type: "disconnect", pd: client.pid }
  chat: (author, msg) -> { type: "chat", :author, :msg }
}

class Client
  next_pid: 1
  new: (@name, @peer, @host) =>
    @pid = Client.next_pid
    Client.next_pid += 1

  send_others: (pid, msg) =>
    msg = @host.serialize_message msg
    for peer, client in pairs @host.clients
      if client.pid != pid
        peer\send msg

  send: (msg) =>
    msg = @host.serialize_message msg
    @peer\send msg

base_handler = MessageHandler {
  (e) => print "default handler"
  chat: (e) =>
    log e.client.name.." typed: ".. e.msg
    print "got chat message"
}

class ServerHost
  num_connected: 0

  new: (ip="*", port=3723) =>
    addr_string = ("%s:%s")\format ip, port
    log "Starting server on "..addr_string
    @host = enet.host_create addr_string

    @clients = {} -- peer to Client
    @all_entities = {}

  serialize_message: (msg) ->
    json.encode msg

  handle_message: (event) =>
    msg = json.decode event.data
    msg.client = @clients[event.peer]
    base_handler.dispatch msg

    if false
      msg_type = deserialized[1]
      ---- CHAT MESSAGES ----
      if msg_type == network.type_chat
        chat_msg = deserialized[3]
        log player.name.." typed: "..chat_msg

        -- Prevent someone from making up the "originator" field
        -- by recreating the chat message
        host\broadcast network.form_chat_msg player.name, chat_msg

        if #chat_msg > 0 and (chat_msg\sub 1,1) == "/"
          if #chat_msg > 1 and (chat_msg\sub -1,-1) == "?"
            event.peer\send network.form_chat_msg "Server", "Help commands to come"

          split_msg = moonscript.util.split chat_msg, ' '

      ---- MOVEMENT MESSAGES ----
      elseif msg_type == network.type_move_from_client
        -- TODO: check boundaries

        client_had_correct_coords = (deserialized[2] != player.x or deserialized[3] != player.y)

        if not client_had_correct_coords
          event.peer\send network.form_set_position_msg player.x, player.y

        player.hero\move tonumber(deserialized[4])
        broadcast_except_to player.pid, network.form_server_move_msg player.hero.eid, deserialized[4]
        
      ---- SPELL MESSAGES ----
      elseif msg_type == network.type_summon
        summon = Entity nil, nil, player, tonumber(deserialized[2]) - 24, tonumber(deserialized[3]) - 25
        all_entities[summon.eid] = summon
        
        host\broadcast network.form_entity_msg summon
        
      ---- DEBUG MESSAGES ----
      elseif msg_type == network.type_debug_click
        broadcast_except_to player.pid, network.form_click_msg player.pid, deserialized[3], deserialized[4]

      else
        log "Couldn't understand message. Type = "..tostring(msg_type)

  run: =>
    while true
      event = @host\service 100 -- arg is timeout, in ms
      if event
        if event.type == "receive" then @handle_message event
        elseif event.type == "connect"
          @num_connected += 1
          client = @create_new_client event.peer
          log client.name.." connected. There are now "..tostring(@num_connected).." connected player(s)."
          
          with client
            \send message.create_player client
            \send_others message.connect client
            \send_others message.chat "Server", .name.." connected"

          -- Send the connecting player's Player to everyone, including the connecting player
          -- event.peer\send network.form_your_player_msg connecting_player
          -- @broadcast_except_to connecting_player.pid, network.form_other_player_msg connecting_player
          
          -- Now that everyone has the Player object, we send out the hero.
          -- All entities need owners, so that's why we needed to send Players first.
          -- @host\broadcast network.form_entity_msg connecting_player.entities[1]

          -- Send the other players' Players and entities to the connecting player
          -- for peer,player in pairs connected_people
          --   if player.pid != connecting_player.pid
          --     event.peer\send network.form_other_player_msg player
          --     for eid,entity in pairs player.entities
          --       event.peer\send network.form_entity_msg entity

        elseif event.type == "disconnect"
          client = @clients[event.peer]
          msg = client.name.." disconnected"
          log msg

          client\send_others message.chat "Server", msg
          client\send_others message.disconnect client

          @clients[event.peer] = nil
          @num_connected -= 1

  create_new_client: (peer) =>
    name = usernames[@num_connected]
    if not name then name = "Player"..@num_connected

    client = Client name, peer, self
    @clients[peer] = client
    
    -- Give that new player a hero
    -- hero = Entity nil, nil, new_player
    -- all_entities[hero.eid] = hero

    client

server = ServerHost!
server\run!

