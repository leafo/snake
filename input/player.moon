
require "support"
require "image"
require "graphics.geometry"
module "input.player", package.seeall

export Player, PlayerController, PlayerSprite

class Animation
  new: (img_name, initial_state, @animation_data) =>
    @sprite = image.load(img_name)\sprited 46, 46, padding_x: 0
    @set_state initial_state
    @stopped = false
    
  update: (dt) =>
    if not @stopped then @time += dt
    
  stop: () =>
    @stopped = true
    
  finished: () =>
    animation_data = @animation_data[@state]
    cycle = animation_data['cycle']
    ms_per_frame = animation_data['ms_per_frame']
    @time * 1000 >= #cycle * ms_per_frame
    
  compute_frame: () =>
    animation_data = @animation_data[@state]
    cycle = animation_data['cycle']
    ms_per_frame = animation_data['ms_per_frame']
    frame_offset = animation_data['frame_offset']
  
    total_cycle_time = #cycle * ms_per_frame
    
    -- Use time instead of @time so that we don't modify anything
    time = @time * 1000 -- convert to ms

    time = math.max(time - ms_per_frame, 0) % total_cycle_time
    index_in_cycle = math.floor(time / ms_per_frame) + 1
    frame = cycle[index_in_cycle]
    
    frame + frame_offset
  
  set_state: (state) =>
    @state = state
    @time = 0
  
  draw: (x, y, dir) =>
    frame = @compute_frame dir
    
    @sprite\draw frame, x, y
    
    @finished!
  
class DirectionalAnimation extends Animation
  directions: { up: 2, right: 3, down: 0, left: 1 }
  
  compute_frame: (dir) =>
    frame = super!
    @directions[dir] * 5 + frame
    
class Player
  speed: 180.0
  new: =>
    @direction = "right"
    @x = 0
    @y = 0
    @animation = DirectionalAnimation "war.png", "stand", { 
      stand: 
        cycle:{0}
        frame_offset:0
        ms_per_frame:100
      walk: 
        cycle:{1,2,1,3,4,3}
        frame_offset:0
        ms_per_frame:100
      attack: 
        cycle:{0,1,2,2}
        frame_offset:20
        ms_per_frame:150
      }
    @controller = PlayerController self

  draw: =>
    finished = @animation\draw @x, @y, @direction

    if finished and @animation.state == "attack"
      @animation\set_state"stand"


directions = {
  up: {"up", "w"}
  left: {"left", "a"}
  down: {"down", "s"}
  right: {"right", "d"}
}

input_to_dir = {}
for dir, keys in pairs directions
  input_to_dir[key] = dir for key in *keys

class PlayerController
  new: (@player) =>
    @moving = {}

  handle_key: (key, unicode) =>
    if key == 'q' and ( @player.animation.state == "walk" or @player.animation.state == "stand" )
      @player.animation\set_state"attack"
      
    dir = input_to_dir[key]
    if dir
      -- find the facing direction
      @facing = dir
      @moving[dir] = true
      
      -- Follow the precedence rules in update
      if @moving.left and @moving.right then @facing = 'left'
      if @moving.up and @moving.down then @facing = 'up'
      true

  handle_key_up: (key, unicode) =>
    dir = input_to_dir[key]
    if dir
      @moving[dir] = false
      true

  update: (dt) =>
    @player.animation\update dt
    
    x, y = 0, 0
    if @moving.left then x -= 1
    elseif @moving.right then x += 1

    if @moving.up then y -= 1
    elseif @moving.down then y += 1
    
    -- The controller handles keydown and keyup, so you may
    -- be facing a direction in which you're no longer moving
    if not @moving[@facing]
      -- Find a direction in which you ARE moving
      @facing = if @moving.left then 'left' else if @moving.right then 'right' else if @moving.up then 'up' else if @moving.down then 'down'

    if x == 0 and y == 0 
      if @player.animation.state == "walk"
        @player.animation\set_state"stand"
      return nil
      
    if @player.animation.state == "stand"
      @player.animation\set_state"walk"

    dir = Vec2(x,y)\normalized! * @player.speed * dt
    if @facing
      @player.direction = @facing

    @player.x += dir.x
    @player.y += dir.y

