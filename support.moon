
export json
export mixin_object, mixin, defaultbl, dump
export mixin_table, smoothstep, extend, copy
export resume

require "conf" if love

json = require "cjson"

require "data"

import concat, insert from table

dump = (->
  require "moonscript.util"
  -- import is always local?
  import dump from moonscript.util
  dump)!

-- mixin methods from an object into self
mixin_object = (object, methods) =>
  for name in *methods
    self[name] = (parent, ...) ->
      object[name](object, ...)

-- mixin class properties into self, call new
mixin = (cls, ...) =>
  meta = getmetatable cls
  for key, val in pairs meta.__index
    self[key] = val if not key\match"^__"
  cls.__init self, ...

-- mixin table values into self
mixin_table = (tbl, keys) =>
  if keys
    for key in *keys
      self[key] = tbl[key]
  else
    for key, val in pairs tbl
      self[key] = val

defaultbl = (t, fn) ->
  if not fn
    fn = tbl
    t = {}
  setmetatable t, {
    __index: (name) =>
      val = fn self, name
      rawset self, name, val
      val
  }

smoothstep = (a, b, t) ->
  t = t*t*t*(t*(t*6 - 15) + 10)
  a + (b - a)*t

extend = (base) =>
  setmetatable self, __index: base

copy = =>
  t = {}
  for key, val in pairs self
    t[key] = val
  t

-- safely resumes a coroutine
resume = (co, ...) ->
  status, err = coroutine.resume co, ...
  if not status and err
    error err
  status

Set = (items) ->
  self = {}
  self[key] = true for key in *items
  self

