module 'constants', package.seeall

export direction

direction = { up: 0, right: 1, down: 2, left: 3 }
