-- TODO:
--
-- particles that show from typing scroll and they shouldn't
--
-- Is it possible to extend a function with mixins and not inheritance?
--    essentially anything is possible, even extending the functionality of a function
--
--  Biggest problem: how do we synchronize server/client movement of NPCs/updates/whatever
--    Leaf says to just send out all of the messages right now
--
-- Work on entities, keeping NPCs/AI in mind. Also, each Player has one Hero.
--    NPCs don't need to receive any network data.
--
-- Stats, life bars, a HUD for your player
--
-- Entities have available abilities
-- Summon: an entity with a TTL
--
-- Use sprite batch for entities?

package.path..=';../?.lua'
require "enet"
require 'network'
require 'shared'
require 'constants'
require 'ui.sequence'
import Sequence from ui.sequence
import Color, Player, Entity, Hero, Poison, Heal from shared
import remove_from_array from shared
import direction from constants

host = enet.host_create!
connected_to_server = false -- will be set to true on the first service
server = nil
my_name = "unnamed" -- this gets filled in by the server

resources_dir = "res/"
images = {}

-- each particle system has a time-to-live. When it is no longer active, this ttl will be set to
-- X, where X is the number of game cycles before it is destroyed. This gives the remaining particles
-- time to die naturally.
--
-- This is a table of key=psystem, value=TTL (TTL=-1 until the psystem is dead)
psystems = {}

-- This is set when you connect and the server sends it
my_player = nil

-- Array of all players, including your own
all_players = {}

-- Table of eid->entity
-- That way, when you get a message pertaining to an entity,
-- you can immediately route it without searching a table
all_entities = {}

-- The box to show your stats in. It's just a mock-up for now.
stats_box = nil

-- The place to show your hero's status effects
effect_window = nil

console = nil

scroll_x, scroll_y = 0,0

find_player_with_pid = (pid) ->
  for player in *all_players
    if player.pid == pid then return player

  return nil

scroll_to_hero = ->
  scroll_x = my_player.hero.x - (love.graphics.getWidth! / 2)
  scroll_y = my_player.hero.y - (love.graphics.getHeight! / 2)
  scroll_x = math.max 0, scroll_x
  scroll_y = math.max 0, scroll_y

class TextLine
  new: (@text) =>
    @color = Color 255, 255, 255
    if @starts_with "Server: "
      @color.g = 128
      @color.b = 128

  update: =>

  starts_with: (str) =>
    len = #str
    --~ print #@text, len, (@text\sub 1,len), str
    return #@text >= len and (@text\sub 1,len) == str

  draw: (x, y) =>
    love.graphics.setColor @color.r, @color.g, @color.b
    love.graphics.print @text, x, y

class Meter
  new: (@max, @value, @x, @y, @width, @height, @r, @g, @b) =>
    if not @value then @value = 0

    @draw_value = 0 -- so that it fills up to begin with

    @seq = Sequence ->
      tween self, 1.4, draw_value: @value

  update: (dt) =>
    @seq\update dt

  set_value: (new_value, max_value) =>
    new_value = math.min(math.max(0, new_value), max_value)
    @max = max_value

    if @value != new_value
      -- Making a new sequence every time with a large enough time value
      -- will result in no difference for very frequent updates
      @seq = Sequence ->
        tween self, 0.5, draw_value: new_value
      @value = new_value

  draw: =>
    border = 2
    love.graphics.setColor 37,37,37,196
    love.graphics.rectangle "fill", @x,@y, @width, @height
    love.graphics.setColor 222,222,222,222

    filled_width = @draw_value / @max * @width
    love.graphics.setColor @r,@g,@b,255
    love.graphics.rectangle "fill", @x,@y, filled_width, @height

    love.graphics.setColor 255,255,255,255
    old_line_width = love.graphics.getLineWidth!
    love.graphics.setLineWidth border
    love.graphics.rectangle "line", @x,@y + (border - 1), @width, @height - (border - 1)
    love.graphics.setLineWidth old_line_width

    text = tostring(@value)..'/'..tostring(@max)
    text_w = love.graphics.getFont!\getWidth text
    text_h = love.graphics.getFont!\getHeight!
    love.graphics.setColor 255,255,255,255
    love.graphics.print text, @x + @width / 2 - text_w / 2, @y + @height / 2 - text_h / 2


healthMeter = Meter 0,0, 300, 0, 200, 20, 255,0,0
manaMeter = Meter 0,0, 300, 22, 200, 20, 56,64,255

set_meter_values = ->
  healthMeter\set_value my_player.hero.stats.life, my_player.hero.stats.max_life
  manaMeter\set_value my_player.hero.stats.mana, my_player.hero.stats.max_mana

--
-- This is a colored textbox that allows newlines.
-- Search [BUG] to read about a current bug in the
-- implementation.
--
class TextBox

  -- All lowercase characters to the rgba values
  -- (^n is reserved for newlines)
  colors: {
    ['r']: {255,0,0,255} -- bright red
    ['g']: {149,200,65,255} -- bright green
    ['b']: {65,149,200,255} -- blue
    ['p']: {202,157,230,255} -- light pink
    ['l']: {100,5,55,255} -- dark purple
    ['o']: {67,90,50,255} -- olive
    ['c']: {197,108,100,255} -- clay
    ['i']: {0,176,0,255} -- lime green
    ['m']: {255,0,200,255} -- magenta
    ['t']: {10,230,230,255} -- teal
    ['w']: {255,255,255,255} -- white
    ['d']: {153,153,77,255} -- gold
    }

  -- This function is used so that you can find the next word
  -- to draw.
  -- Returns two things:
  -- 1. The next printable word
  -- 2. The index that the printable word ends at. This isn't necessarily
  --   equal to the length of the printable word, because it may contain color
  --   characters (e.g. ^R)
  --
  -- This function has similar logic as the other parsing code
  next_printable_word: (text) ->
    word = ''
    length = string.len text
    i = 1

    -- This function won't return on spaces until it finds a non-space
    found_a_printable_char = false
    while i <= length
      c = text\sub i, i

      if c == " " and found_a_printable_char then return word,i
      if c == "^"
        -- is there another character?
        if i < length
          -- get the next character
          i += 1
          d = (text\sub i, i)\lower!

          -- If it's a color, then we don't add it to our printable word
          if TextBox.colors[d] then c = nil

      if c
        word..=c
        found_a_printable_char = true
      i += 1
    return word,i

  new: (@text, @total_width) =>
    @prepare!

  draw: (x,y) =>
    padding = 2
    love.graphics.setColor 37,37,37,128
    love.graphics.rectangle "fill", x,y, @total_width + padding * 2, @actual_height + padding * 2
    love.graphics.setColor 222,222,222,222
    love.graphics.rectangle "line", x,y, @total_width + padding * 2, @actual_height + padding * 2

    love.graphics.setColor 255,255,255,255
    love.graphics.draw @canvas, x + padding,y + padding

  -- Makes a canvas and draws all of the text to it
  prepare: =>
    -- Note: due to the alpha bug (https://bitbucket.org/rude/love/issue/98/ ),
    -- I don't make a temporary canvas here and then paste to a canvas of the correct size
    -- (a temporary canvas would be needed because 'prepare' sets @actual_height).
    -- If I did that, the alpha bug would essentially get duplicated and you would
    -- end up with very fuzzy text.
    -- Anyway, because I'm NOT doing that, I make the canvas the height of the
    -- screen.
    --
    -- A better way to go about the precomputation might be to parse the text and
    -- store a list of commands like "set color (r,g,b), print 'abcdef',
    -- carriage return, print 'ghijkl', etc."
    --
    -- Also note: it's faster to clear the canvas than to make a new one.
    @canvas = love.graphics.newCanvas @total_width,love.graphics.getHeight!
    love.graphics.setRenderTarget @canvas
    
    if not @total_width then @total_width = 100
    -- For now, you just have ^X to represent a color.
    -- ^     not enough data
    -- ^^    a caret
    -- ^X    a color (possibly invalid, in which case a caret is drawn)

    love.graphics.setColor 255,255,255,255
    x,y = 0,0

    -- Keep track of how much space we have with which to print a string
    avail_width = @total_width

    carriage_return = ->
      avail_width = @total_width
      x = 0
      y += love.graphics.getFont!\getHeight!

    -- [BUG] (don't care about fixing this right now)
    -- If a line is too long to fit because there is a trailing space,
    -- then the space can be discarded, but I'm not doing that, so you may
    -- end up with line breaking like the following:
    -----------------------------
    -- The first line           -
    --                          - <--- this line contains a single space which should just be rejected
    -- TheThirdLineAllCondensed -
    -----------------------------
    text_copy = @text -- copy the text so that we don't destroy the original while lopping off words
    while true
      -- Stop when we've printed all of the text
      if (string.len text_copy) == 0 then break

      -- Get the next printable word so that we can determine if we need to
      -- skip to the next lien.
      next_word, num_chars = TextBox.next_printable_word text_copy

      -- This contains even non-printable characters
      next_word_all_chars = text_copy\sub 1, num_chars
      text_copy = text_copy\sub num_chars + 1, string.len text_copy
      width_needed = love.graphics.getFont!\getWidth next_word

      -- If we don't have enough space, do a carriage return
      if width_needed > avail_width then carriage_return!

      -- Now we can draw the text
      i = 1
      length = string.len next_word_all_chars
      while i <= length
        c = next_word_all_chars\sub i, i

        if c == "^"
          -- is there another character?
          if i < length
            -- get the next character
            i += 1
            d = (next_word_all_chars\sub i, i)\lower!

            if d == 'n'
              carriage_return!
              c = nil
            else
              color = TextBox.colors[d]

              if color
                love.graphics.setColor color[1],color[2],color[3],color[4]
                c = nil

        if c
          -- Print it a couple of times so that
          -- it doesn't look so dim
          -- https://bitbucket.org/rude/love/issue/98/
          love.graphics.print c, x, y
          love.graphics.print c, x, y

          c_size = love.graphics.getFont!\getWidth c
          x += c_size
          avail_width -= c_size

          -- If we're printing something like "aaaaaaaaaa", we may need to
          -- split again.
          if x > @total_width then carriage_return!

        i += 1
    @actual_height = y + love.graphics.getFont!\getHeight!
    love.graphics.setRenderTarget!

-- A TextBox with an associated Entity, used for displaying stats
class StatsBox extends TextBox
  new: (@entity) =>
    super @get_str!, love.graphics.getWidth! - console.width - 2
    
    @entity.stats_box = self
  
  update_text: =>
    @text = @get_str!
    @prepare!
  
  get_str: =>
    s = @entity.stats
    b = @entity.buffed_stats
    buff_str = if b.str == 0 then '' else if b.str > 0 '^g+'..b.str else '^r'..b.str
    buff_agi = if b.agi == 0 then '' else if b.agi > 0 '^g+'..b.agi else '^r'..b.agi
    buff_int = if b.int == 0 then '' else if b.int > 0 '^g+'..b.int else '^r'..b.int
    
    stat_str = ''
    stat_str..="^w"..@entity.name.." lv. "..tostring(s.level).."^n"
    stat_str..="^rLife: "..tostring(s.life + b.life)..'/'..tostring(s.max_life + b.max_life).."^n"
    stat_str..="^bMana: "..tostring(s.mana + b.mana)..'/'..tostring(s.max_mana + b.max_mana).."^n"
    stat_str..="^dStrength: ^w"..tostring(s.str)..' '..buff_str.."^n"
    stat_str..="^dAgility: ^w"..tostring(s.agi)..' '..buff_agi.."^n"
    stat_str..="^dIntelligence: ^w"..tostring(s.int)..' '..buff_int
    
    return stat_str
    
  
-- Returns true if px,py is in the rect specified by the last four coordinates
point_in_rect = (px, py, x, y, w, h) ->
  if px < x or px > x + h or py < y or py > y + h return false else return true

class Button
  new: (@x, @y, @width, @height, @func, @r, @g, @b) =>
    @hovering = false

  -- returns true if it did indeed capture this event,
  -- false if it didn't (so that something else could capture it)
  --
  -- x and y are in screen coordinates
  handle_lmb_down: (x, y) =>
    if point_in_rect( x, y, @x, @y, @width, @height )
      @func self
      return true
    return false
  handle_mouse_move: (m_x, m_y) =>
    --~ This is probably masking a bug where hovering isn't set to false all the time because of how events are handled
    if point_in_rect( m_x, m_y, @x, @y, @width, @height )
      @hovering = true
    else
      @hovering = false

  draw: =>
    if @hovering
      love.graphics.setColor @r, @g, @b, 200
    else
      love.graphics.setColor @r, @g, @b, 100

    love.graphics.rectangle "fill", @x, @y, @width, @height
    love.graphics.setColor 255, 255, 255
    love.graphics.rectangle "line", @x, @y, @width, @height

-- This button is used to draw effects. It knows how to draw such that
-- you can easily see the time remaining on an effect.
class EffectButton extends Button
  new: (@width, @height, @func, @r, @g, @b, @image, @sx, @sy, @effect) =>
    -- X and Y should be set by the effect window, so pass in -3,-3 so that
    -- it's obvious when they're not positioned correctly
    super -3,-3, @width, @height, @func, @r, @g, @b
    
  draw: =>
    love.graphics.setColor 255,255,255
    love.graphics.draw @image, @x, @y, 0, 0.5, 0.5

    -- Clip drawing to the button
    love.graphics.setScissor @x, @y, @width, @height
    
    -- Draw an arc to represent remaining time
    love.graphics.setColor 37,37,37,150
    radius = (@width / 2) / (1 / math.sqrt(2) )
    angle = (@effect.ttl / @effect.total_ttl * 2 * math.pi)
    love.graphics.arc "fill", @x + @width / 2,@y + @height / 2, radius, math.pi / 2, angle + math.pi / 2
    
    -- Restore settings
    love.graphics.setScissor!
    love.graphics.setColor 255,255,255,255

-- This will display the buffs/debuffs of a given entity.
-- It makes the effects into buttons so that you can interact with
-- them (e.g. view tooltip, click to cancel)
class EffectWindow
  new: (@entity) =>
    -- Associate this window with the entity so that it knows to update this
    -- every time a buff is added or removed.
    @entity.effect_window = self
    
    -- Array of buttons (each button contains an effect)
    @buttons = {}
  
  -- The entity associated with this window is in charge of calling this function
  -- every time an effect is added.
  add_effect: (effect) =>
    effect_button = EffectButton 32,32, ( -> print "hi"), 255,255,255, images[effect.name], 0.5, 0.5, effect
    table.insert @buttons, effect_button
    
    @set_button_coords!
  
  remove_effect: (effect_to_remove) =>
  
    i, index = 1, nil
    for button in *@buttons
      if button.effect == effect_to_remove then
        index = i
        break
      i += 1

    if index then table.remove @buttons, index
    
    @set_button_coords!
  
  -- Positions the buttons. For now, it positions them in a line. Maybe
  -- eventually we may want to put them in two rows or change their size...
  set_button_coords: =>
    x = 5
    for button in *@buttons
      button.x = x
      button.y = 50
      x += 33
  
  update: =>
  
  draw: (x,y) =>
    for effect,button in pairs @buttons
      button\draw!

make_part_system = (x, y) ->
  -- No clue why particle systems need this repositioning code
  x /= 2
  y /= 2
  psystem = love.graphics.newParticleSystem images.particle, 50
  psystem\setEmissionRate 15
  psystem\setPosition x, y
  psystem\setOffset 0,0
  psystem\setLifetime 1
  psystem\setParticleLife 1.0
  psystem\setSpread 30
  psystem\setSpeed 60
  psystem\setDirection 0.0
  psystem\setColors 255,255,255,255, 0,0,255,255
  psystem\setSizes 1, 2, 1
  psystem\start!

  psystems[#psystems + 1] = {psystem, -1}

-- Initial args aren't working, so I'm just going to make a second function for a different kind of particle system
make_part_system2 = (x, y, i) ->
  -- No clue why particle systems need this repositioning code
  x /= 2
  y /= 2
  psystem = love.graphics.newParticleSystem images.particle, 500
  psystem\setEmissionRate 150
  psystem\setPosition x, y
  psystem\setOffset 0,0
  psystem\setLifetime 1
  psystem\setParticleLife 1.0
  psystem\setSpread 1
  psystem\setSpeed 90
  psystem\setDirection math.random(1,360)
  r = math.floor(1000 / i) % 255
  g = math.floor(10000000 / i) % 255
  b = math.floor(1000000 / i) % 255
  psystem\setColors r,g,b,255, 0,0,0,255
  psystem\setSizes 1, 5, 1
  psystem\start!

  psystems[#psystems + 1] = {psystem, -1}

find_last_space = (s) ->
  space = string.find s, ' '
  if not space then return nil
  while true
    space2 = string.find s, ' ', space + 1
    if not space2 then return space
    space = space2

class ConsoleWindow
  text_entry_height: 20
  key_chars: {
    ['a']:'A', ['b']:'B', ['c']:'C', ['d']:'D', ['e']:'E', ['f']:'F', ['g']:'G'
    ['h']:'H', ['i']:'I', ['j']:'J', ['k']:'K', ['l']:'L', ['m']:'M', ['n']:'N'
    ['o']:'O', ['p']:'P', ['q']:'Q', ['r']:'R', ['s']:'S', ['t']:'T', ['u']:'U'
    ['v']:'V', ['w']:'W', ['x']:'X', ['y']:'Y', ['z']:'Z'
    ['1']:'!', ['2']:'@', ['3']:'#', ['4']:'$', ['5']:'%', ['6']:'^', ['7']:'&'
    ['8']:'*', ['9']:'(', ['0']:')', [',']:'<', ['.']:'>', ['/']:'?', [';']:':'
    ["'"]:'"', ['[']:'{', [']']:'}', ['-']:'_', ['=']:'+', [' ']:' '
    }
  new: (@x, @y, @width, @height) =>
    @text_lines = { TextLine "Press ~ to close this" } -- remember, in lua, this is really "1:<object>"
    @visible = true
    bwidth, padding = 50, 5

    @rbutton = Button @x + @width - bwidth - padding, @y + padding, bwidth, bwidth, ((b) -> @log "Your hero is at "..tostring(my_player.hero.x)..','..tostring(my_player.hero.y)), 128, 0, 0
    @gbutton = Button @rbutton.x, @y + @rbutton.height + padding, bwidth, bwidth, ((b) -> @log "Clicked green button"), 0, 128, 0
    @type_text = ''
    @blink = 0
    @separator_line_y = @y + @height - @text_entry_height

    -- When false, the console only handles 'enter' (and '~' if I haven't disabled that yet)
    @is_typing = false
  -- can't call this until after love was loaded
  initialize: =>
    @font_height = love.graphics.getFont!\getHeight!
    @max_num_text_lines = math.floor( (@height - @text_entry_height) / @font_height)
    @set_cursor_to_end!
  log: (text) =>
    if #@text_lines >= @max_num_text_lines
      @text_lines = [item for item in *@text_lines[2:]]
    @text_lines[#@text_lines + 1] = TextLine text
  update: =>
    @blink += 1
    if @blink == 30 then @blink = 0
    for text_line in *@text_lines
      text_line\update!
  set_cursor_to_end: =>
    @cursor_x = 7 + love.graphics.getFont!\getWidth @type_text

  in_type_mode: =>
    return @visible and @is_typing
  -- Draw the background
  draw_bg: =>
    love.graphics.setColor 0, 0, 0, 100
    love.graphics.rectangle "fill", @x, @y, @width, @height

  draw_fg: =>
    love.graphics.setColor 255, 255, 255
    love.graphics.rectangle "line", @x, @y, @width, @height
  -- see Button.handle_lmb_down
  handle_lmb_down: (x, y) =>
    if @rbutton\handle_lmb_down x, y return true
    if @gbutton\handle_lmb_down x, y return true

    return false

  handle_mouse_move: (m_x, m_y) =>
    if @rbutton\handle_mouse_move m_x, m_y return true
    if @gbutton\handle_mouse_move m_x, m_y return true

    return false


  handle_key: (key, unicode, shift_is_held, ctrl_is_held) =>

    if key == 'escape' and @is_typing
      @is_typing = false
      @type_text = ''
      return true

    if key == 'return' or key == 'kpenter'
      if @is_typing
        if not connected_to_server
          @log @type_text.." (you are not connected to the server)"
        else
          @type_text = moonscript.util.trim(@type_text)
          if string.len(@type_text) > 0
            server\send network.form_chat_msg "unnamed", @type_text
        @type_text = ''

      @is_typing = not @is_typing

    if not @is_typing then return nil

    shift_char = @key_chars[key]

    if key == 'backspace' and #@type_text > 0
      end_index = #@type_text - 1
      if ctrl_is_held
        last_space = find_last_space @type_text
        if last_space == nil
          end_index = 0
        else
          end_index = (find_last_space @type_text)
          if (@type_text\sub end_index, end_index) == ' ' then end_index -= 1

      @type_text = @type_text\sub 1, end_index
      return true

    if shift_char != nil
      if shift_is_held
        @type_text ..= shift_char
      else
        @type_text ..= key

      make_part_system @cursor_x, @separator_line_y + @text_entry_height / 2
      return true

    return false

  -- Draw the text entry at the bottom
  draw_text_entry: =>
    -- Draw the line separating it from the output
    love.graphics.setColor 255, 255, 255
    old_line_width = love.graphics.getLineWidth!
    love.graphics.setLineWidth 3
    love.graphics.line @x, @separator_line_y, @x + @width, @separator_line_y
    love.graphics.setLineWidth old_line_width

    -- Draw the text
    love.graphics.print @type_text, @x + 5, @separator_line_y + 4

    -- Draw the cursor
    if @is_typing
      if @blink < 25
        @cursor_x = @x + 7 + love.graphics.getFont!\getWidth @type_text

        love.graphics.line @cursor_x, @separator_line_y + 1, @cursor_x, @y + @height - 1


  draw: =>
    if not @visible then return nil

    @draw_bg!

    index = 0
    for text_line in *@text_lines
      text_line\draw @x, @y + index * @font_height
      index += 1

    @draw_text_entry!

    @rbutton\draw!
    @gbutton\draw!

    @draw_fg!

  toggle_vis: =>
    @visible = not @visible
  show: =>
    @visible = true

c_height = 200
console = ConsoleWindow 1,love.graphics.getHeight! - (c_height + 1),600,c_height

load_image = (fname) ->
  love.graphics.newImage resources_dir..fname -- implicit return


love.load = ->
  images.sprite = load_image "char.png"
  images.sprite2 = load_image "char2.png"
  images.particle = load_image "p.png"
  images.arena = load_image "arena.png"
  images.poison = load_image "poison.png"
  images.heal = load_image "heal.png"
  love.graphics.print '',0,0 -- print an empty string so that the font will be initialized
  console\initialize!

  --~ for k,v in pairs getmetatable(psystem).__index
    --~ print k

  server = host\connect network.ip..":"..network.port
  math.randomseed( os.time() )
  math.random()
  math.random()
  math.random()
  --~ music = love.audio.newSource("res/"..tostring(math.random 1,3)..".ogg")
  --~ love.audio.setVolume(0.1)
  --~ love.audio.play(music)

handle_network_msgs = ->
  event = 1
  while event
    event = host\service 0 -- smallest possible timeout
    if event
      if event.type == "connect"
        connected_to_server = true
        print "Connected to", event.peer
        console\log "Connected to server: "..tostring(event.peer)
      elseif event.type == "receive"
        deserialized = network.deserialize event.data
        msg_type = deserialized[1]

        ---- CHAT MESSAGES ----
        if msg_type == network.type_chat
          console\log deserialized[2]..": "..deserialized[3]


        ---- WORLD MESSAGES ----
        elseif msg_type == network.type_your_player
          my_player = Player.deserialize deserialized[2]
          console\log "Received your player: "..my_player\serialize!
          table.insert all_players, my_player
        elseif msg_type == network.type_other_player
          other_player = Player.deserialize deserialized[2]
          console\log "Received other player: "..other_player\serialize!
          table.insert all_players, other_player

        elseif msg_type == network.type_populate_hero
          hero = Hero.deserialize deserialized[2], find_player_with_pid
          console\log "Received a  hero: "..hero\serialize!
          all_entities[hero.eid] = hero

          -- my_player.hero will be set upon deserializing the hero
          if hero == my_player.hero
            scroll_to_hero!
            set_meter_values!

            stats_box = StatsBox hero
            effect_window = EffectWindow hero

        ---- MOVEMENT MESSAGES ----
        elseif msg_type == network.type_move_from_server
          eid = tonumber(deserialized[2])
          dir = tonumber(deserialized[3])
          entity_to_move = all_entities[eid]
          if entity_to_move
            --~ console\log "X,Y="..tostring(player_to_move.x)..','..tostring(player_to_move.y)..' dir='..tostring(dir)
            entity_to_move\move dir
          else
            console\log "[move] Couldn't find entity with eid = "..tostring(eid)

        elseif msg_type == network.type_set_position
          x = tonumber(deserialized[2])
          y = tonumber(deserialized[3])
          my_player.x = x
          my_player.y = y
          console\log "Server adjusted your position to X,Y="..tostring(my_player.x)..','..tostring(my_player.y)

        ---- DEBUG MESSAGES ----
        elseif msg_type == network.type_debug_click
          pid = tonumber(deserialized[2])
          x = tonumber(deserialized[3])
          y = tonumber(deserialized[4])
          make_part_system2 x,y,pid

        ---- DISCONNECT ----
        elseif msg_type == network.type_disconnect
          pid = tonumber(deserialized[2])
          player_to_disconnect = find_player_with_pid pid
          if player_to_disconnect
            console\log "Server: "..player_to_disconnect.name.." disconnected."
            remove_from_array all_players, player_to_disconnect

            -- Remove all of the entities
            for entity in *player_to_disconnect.entities
              all_entities[entity.eid] = nil

          else
            console\log "[disc] Couldn't find player with pid = "..tostring(pid)

        ---- UNKNOWN MESSAGE ----
        else
          err = "Couldn't understand message. "
          if not deserialized
            err..= "deserialized is nil."
          else
            err..="Type = "..tostring(msg_type)
          console\log err

        --~ console\log "Got message of type="..tostring(msg_type)
        --~ print("Got message: ", event.data, event.peer)

love.update = (dt) ->

  moved = nil

  if (love.keyboard.isDown "up") or ((love.keyboard.isDown 'w') and (not console\in_type_mode!))
    server\send network.form_client_move_msg my_player.hero.x, my_player.hero.y, direction.up
    my_player.hero\move direction.up
    moved = true
  if (love.keyboard.isDown "right") or ((love.keyboard.isDown 'd') and (not console\in_type_mode!))
    server\send network.form_client_move_msg my_player.hero.x, my_player.hero.y, direction.right
    my_player.hero\move direction.right
    moved = true
  if (love.keyboard.isDown "down") or ((love.keyboard.isDown 's') and (not console\in_type_mode!))
    server\send network.form_client_move_msg my_player.hero.x, my_player.hero.y, direction.down
    my_player.hero\move direction.down
    moved = true
  if (love.keyboard.isDown "left") or ((love.keyboard.isDown 'a') and (not console\in_type_mode!))
    server\send network.form_client_move_msg my_player.hero.x, my_player.hero.y, direction.left
    my_player.hero\move direction.left
    moved = true

  if moved
    scroll_to_hero!

  handle_network_msgs!

  console\update!

  m_x, m_y = love.mouse.getPosition!
  console\handle_mouse_move m_x, m_y

  dead = {}
  for k,v in pairs psystems
    v[1]\update dt
    if not v[1]\isActive! and v[2] == -1
      v[1]\stop!
      v[2] = 100
    if v[2] != -1
      v[2] = v[2] - 1
      if v[2] == 0
        table.insert dead, k

  for d in *dead do remove_from_array psystems, d

  if my_player and my_player.hero
    my_player.hero\update dt
    set_meter_values!
  healthMeter\update dt
  manaMeter\update dt

-- key = character of the key pressed
-- unicode = the unicode number of the key pressed
love.keypressed = (key, unicode) ->
  shift_is_held = (love.keyboard.isDown "rshift") or (love.keyboard.isDown "lshift")
  ctrl_is_held = (love.keyboard.isDown "rctrl") or (love.keyboard.isDown "lctrl")

  if console.visible
    if console\handle_key key, unicode, shift_is_held, ctrl_is_held return nil
  else
    -- you can also make it visible and start typing at the same time
    -- by pressing enter
    if key == 'return' or key == 'kpenter'
      console\toggle_vis!
      console.is_typing = true

  if key == "p"
    my_player.hero\apply_effect Poison, 1
  if key == "h"
    my_player.hero\apply_effect Heal, 1
  if key == "`"
    console\toggle_vis!

  if key == "escape"
    love.quit!

love.mousepressed = (x, y, button) ->
  world_x, world_y = x + scroll_x, y + scroll_y
  if button == 'l' -- that's an L, not a 1
    console\handle_lmb_down x, y -- screen coords, not world coords
  if button == 'r'
    server\send network.form_click_msg -1, world_x, world_y
    make_part_system2 world_x,world_y, my_player.pid
  if button == 'm'
    server\send network.form_summon_msg world_x, world_y


love.quit = ->
  server\disconnect!
  host\flush!

  os.exit!

love.draw = ->

  love.graphics.draw images.arena, 0 - scroll_x, 0 - scroll_y

  -- Draw all players
  for eid,entity in pairs(all_entities)
    entity\draw (if (eid %2 == 1) then images.sprite else images.sprite2), scroll_x, scroll_y
    
  console\draw!

  for psystem in *psystems do love.graphics.draw psystem[1], psystem[1]\getX! - scroll_x, psystem[1]\getY! - scroll_y

  if stats_box
    -- Update the stats box to show the latest stats. The alternative to updating
    -- every loop would be to make a function like modify_buffed_stats(stat, val)
    -- in one of the entity classes that wraps this update call.
    stats_box\update_text!
    stats_box\draw love.graphics.getWidth! - stats_box.total_width, love.graphics.getHeight! - stats_box.actual_height - 5
  
  if effect_window
    effect_window\draw 0,0

  healthMeter\draw!
  manaMeter\draw!

