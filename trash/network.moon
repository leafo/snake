module 'network', package.seeall
require 'moonscript.util'

export form_chat_msg, form_your_player_msg, form_other_player_msg, form_client_move_msg, form_server_move_msg, form_set_position_msg, form_disconnect_msg
export form_click_msg, form_hero_msg, form_summon_msg
export deserialize
export type_chat, type_your_player, type_other_player, type_move_from_client, type_move_from_server, type_set_position, type_disconnect
export type_debug_click, type_populate_hero, type_summon
export ip, port

-- Address information
ip = "50.46.155.69"
port = "3723"

-- Separator for serialized messages. Note that nothing is escaped,
-- so message data may not contain this character.
separator = '~'

-- Forward declarations:
msg_to_num = nil

-- Types for messages. Every message sent across the network needs a type and
-- then some amount of data.


-- Chat message
-- Sent from: client/server
--
-- Arguments:
--  originator: the originator of the message
--  msg: the message itself
--
-- Remarks:
--  When sent from client->server, originator is ignored; the server fills in
--  that section.
--
--  Also note that everyone will receive chat messages in the same order because
--  there is no client-side prediction.
type_chat = "chat"
form_chat_msg = (originator, msg) ->
  return tostring(msg_to_num[type_chat])..separator..originator..separator..msg

-- Populate your player
-- Sent from: server
--
-- Arguments:
--  player: the serialized player object
--
-- Remarks:
--  This is only ever sent to a connecting player. Other players will get
--  "other plyer" objects
type_your_player = "your player"
form_your_player_msg = (player) ->
  return tostring(msg_to_num[type_your_player])..separator..player\serialize!

-- Populate another player
-- Sent from: server
--
-- Arguments:
--  player: the serialized player object
--
-- Remarks:
--  When a client connects, everyone is sent the new client as "other player"
type_other_player = "other player"
form_other_player_msg = (player) ->
  return tostring(msg_to_num[type_other_player])..separator..player\serialize!

-- Populate a hero (may be yours or someone else's)
-- Sent from: server
--
-- Arguments:
--  hero: the serialized hero object
type_populate_hero = "populate hero"
form_hero_msg = (hero) ->
  return tostring(msg_to_num[type_populate_hero])..separator..hero\serialize!

-- Move
-- Sent from: client
--
-- Arguments:
--  x: the 'x' coordinate where the player thinks he is
--  y: the 'y' coordinate where the player thinks he is
--  dir: the direction to move in (this is exported from constants)
--
-- Remarks:
--  1. If the coordinates were wrong, then the server will send another message
--      back that corrects this client.
--  2. The server doesn't need to make use of x,y, so it sends a different message
--      to other players.
type_move_from_client = "move from client"
form_client_move_msg = (x, y, dir) ->
  return tostring(msg_to_num[type_move_from_client])..separator..tostring(x)..separator..tostring(y)..separator..tostring(dir)

-- Move
-- Sent from: server
--
-- Arguments:
--  eid: the entity ID of the moving entity
--  dir: the direction to move in (this is exported from constants)
--
-- Remarks:
--  All players receiving this should be synchronized solely by virtue of reliable
--   messages.
type_move_from_server = "move from server"
form_server_move_msg = (eid, dir) ->
  return tostring(msg_to_num[type_move_from_server])..separator..tostring(eid)..separator..tostring(dir)

-- Set position
-- Sent from: server
--
-- Arguments:
--  x: 'x' coordinate
--  y: 'y' coordinate
--
-- Remarks:
--  This is sent when the client reported his coordinates incorrectly. The server
--   uses this message to correct him.
type_set_position = "correct position"
form_set_position_msg = (x, y) ->
  return tostring(msg_to_num[type_set_position])..separator..tostring(x)..separator..tostring(y)

-- Disconnect
-- Sent from: server
--
-- Arguments:
--  pid: pid of disconnecting player
type_disconnect = "disconnect player"
form_disconnect_msg = (pid) ->
  return tostring(msg_to_num[type_disconnect])..separator..tostring(pid)

-- Player click (debug function)
-- Sent from: client/server
--
-- Arguments:
--  pid: player that caused the action (filled in by server)
--  x: x coord of click
--  y: y coord of click
type_debug_click = "debug click"
form_click_msg = (pid, x, y) ->
  return tostring(msg_to_num[type_debug_click])..separator..tostring(pid)..separator..tostring(x)..separator..tostring(y)

-- Summon
-- Sent from: client
--
-- Arguments:
--  x: x coord of click
--  y: y coord of click
type_summon = "summon at"
form_summon_msg = (x, y) ->
  return tostring(msg_to_num[type_summon])..separator..tostring(x)..separator..tostring(y)

-- Every message has a type and a number. The number is
-- a condensed way to represent the type, so there are two tables
-- for quick access in either direction.
--
-- This way, messages can get sent as "1 1" instead of "move_direction 1"
--
-- Note: the message itself is essentially useless...
-- Pro: you can tell what message you don't understand because this string is there
-- Con: if you don't change it, two messages will be conflated.
num_to_msg = {type_chat, type_your_player, type_other_player, type_move_from_client, type_move_from_server, type_disconnect, type_debug_click, type_populate_hero, type_summon}
msg_to_num = {}
for k,v in pairs num_to_msg
  msg_to_num[v] = k

-- Deserialize data.
-- Returns a table in the following form:
--   ret_val[1] -> message type. This is a type_XXXXX variable from above, e.g.
--                  "chat". Warning: this may be nil for unrecognized types.
--   ret_val[2...n] -> the parameters of that message. See form_XXXXX_msg
--                  for each specific message's format.
--
-- WARNING: ALL ENTRIES IN THE RETURN TABLE WILL BE STRINGS!
-- Make sure to do tonumber() appropriately.
deserialize = (data) ->
  split_data = moonscript.util.split data, separator

  -- Overwrite the numeric form of the type with the actual form
  split_data[1] = num_to_msg[tonumber(split_data[1])]
  return split_data
