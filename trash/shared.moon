module 'shared', package.seeall
package.path..=';../?.lua'
require 'moonscript.util'
require 'constants'
require 'support'
require 'cjson'

import encode, decode from cjson
import direction from constants

export Color, Player, Entity, Hero, Poison, Heal
export remove_from_array

Stats = nil

remove_from_array = (the_array, item_to_remove) ->
  -- Find the index of the item, then do table.remove on it
  i, index = 1, nil
  for item in *the_array
    if item == item_to_remove then
      index = i
      break
    i += 1

  if index then table.remove the_array, index

class Color
  new: (@r, @g, @b) =>

  __tostring: =>
    return 'RGB=('..tostring(@r)..','..tostring(@g)..','..tostring(@b)..')'

-- This class represents a buff or debuff.
--
-- The biggest functional difference between these in other games is that you
-- can usually cancel buffs.
class StatusEffect

  -- level: the level of the effect
  -- entity: the entity who has the effect
  -- is_buff: true if this is a buff
  -- icon: the icon to show on the HUD
  -- character_graphic: unused for now, but this'll be a graphic that shows on the character
  -- ttl: if this is a time-based effect, then this is the amount of time that this has before it expires
  -- name: the name of the effect
  -- proc_rate: how often this effect should proc (only used for time-based effects)
  new: (@level, @entity, @is_buff, @icon, @character_graphic, @ttl, @name, @proc_rate) =>

    -- MEMOIZE the original ttl
    @total_ttl = @ttl

    -- time increases every time you update this
    @time = 0
    @is_alive = true
    
    -- Every effect keeps track of how much it modified the entity's stats. That
    -- way, when this effect goes away, you can revert its changes without
    -- wiping out other effects' changes.
    @buffed_stats = Stats.get_blank_stats!

  update: (dt) =>
    @time += dt
    @ttl -= dt

    while @time > @proc_rate
      @proc!
      @time -= @proc_rate

    if @ttl and @ttl <= 0
      @is_alive = false

  -- Each individual effect needs to implement proc
  proc: =>
    print "You called StatusEffect.proc. You need to write a proc function for "..tostring(@name)

  -- This is called when the entity acknowledges the death of this effect
  died: =>
    -- Reset any buffed stats from this effect
    @entity.buffed_stats -= @buffed_stats

-- Basic DoT debuff
class Poison extends StatusEffect
  is_buff: false
  icon: 'poison'
  name: 'poison'
  character_graphic: nil

  new: (@level, @entity) =>
    ttl = 3 + @level
    super(@level, @entity, Poison.is_buff, Poison.icon, Poison.character_graphic, ttl, Poison.name, 1)

  update: (dt) =>
    super(dt)

  proc: =>
    @entity.stats\modify_life -1 * @level
    
    @entity.buffed_stats -= @buffed_stats
    @buffed_stats.int -= 1
    @entity.buffed_stats += @buffed_stats

-- Basic HoT buff
class Heal extends StatusEffect
  is_buff: true
  icon: 'heal'
  name: 'heal'
  character_graphic: nil

  new: (@level, @entity) =>
    ttl = 3 + @level
    super(@level, @entity, Heal.is_buff, Heal.icon, Heal.character_graphic, ttl, Heal.name, 1)

  update: (dt) =>
    super(dt)

  proc: =>
    @entity.stats\modify_life @level
    
    @entity.buffed_stats -= @buffed_stats
    @buffed_stats.str += 1
    @entity.buffed_stats += @buffed_stats

class Stats

  stat_keys:{ 'level', 'life', 'max_life', 'mana', 'max_mana', 'str', 'agi', 'int'}

  -- Doesn't contain experience since summons don't need that, and this class
  -- is to be shared by all entities with stats
  new: (@level, @life, @max_life, @mana, @max_mana, @str, @agi, @int) =>
    if not @level then @level = 1
    if not @life then @life = @max_life
    if not @mana then @mana = @max_mana

  -- Adds value to life, capping it at 0 and @max
  modify_life: (value) =>
    @life += value
    @life = math.min(math.max(0, @life), @max_life)

  -- obj is any class that has stats mixed in
  serialize: =>
    return encode {@level, @life, @max_life, @mana, @max_mana, @str, @agi, @int}

  -- Returns a Stats object with all fields set to 0
  get_blank_stats: ->
    return Stats 0,0,0,0,0,0,0,0
  
  -- Add another stats object (this modifies self)
  __add: (stats) =>
    for key in *Stats.stat_keys
      self[key] += stats[key]
    self
      
  -- Subtract another stats object (this modifies self)
  __sub: (stats) =>
    for key in *Stats.stat_keys
      self[key] -= stats[key]
    self
  deserialize: (serialized) ->
    decoded = decode serialized
    level = tonumber(decoded[1])
    life = tonumber(decoded[2])
    max_life = tonumber(decoded[3])
    mana = tonumber(decoded[4])
    max_mana = tonumber(decoded[5])
    str = tonumber(decoded[6])
    agi = tonumber(decoded[7])
    int = tonumber(decoded[8])

    return Stats level, life, max_life, mana, max_mana, str, agi, int

class Entity
  next_eid: 1
  -- Arguments:
  -- owner: a Player object. Cannot be nil. When serialized, only the pid is sent across.
  --    The owner that you pass in will automatically be given this entity.
  -- eid: entity ID. If nil, the next unassigned eid will be used.
  new: (@x, @y, @eid, @owner) =>
    if not @owner
      print "Warning! Created an entity with a nil owner!"
    else
      @owner\give_entity self

    if not @x then @x = math.random 1,700
    if not @y then @y = math.random 1,500
    if not @eid
      @eid = Entity.next_eid
      Entity.next_eid += 1

    -- Array of status effects
    @status_effects = {}

entity_mixins = {

  move: (dir) =>
    if dir == direction.up then @y -= 5
    if dir == direction.right then @x += 5
    if dir == direction.down then @y += 5
    if dir == direction.left then @x -= 5

    @facing = dir

  draw_text_label: (text, quad, scroll_x, scroll_y) =>
    font_height = love.graphics.getFont!\getHeight!
    font_width = love.graphics.getFont!\getWidth text
    qx,qy,qw,qh = quad\getViewport!
    x_offset = (font_width - qw) / 2

    love.graphics.setColor 255,0,0
    love.graphics.print text, @x - x_offset - scroll_x, @y - font_height - scroll_y
}

class Hero extends Entity
  random_names: {'Volmog', 'Balkhnarb', 'Lurgagal', 'Oggar', 'Lurgubal', 'Balkhmog', 'Tarkhubal', 'Bolnarb', 'Tarkhrod', 'Durbarod'}

  new: (@x, @y, @eid, @owner, @name, @stats, @experience) =>
    super @x, @y, @eid, @owner

    -- TODO: figure out stats based on hero type (assume lv. 1)
    if not @stats
      @stats = Stats 1,nil,100,nil,50,12,14,13
      
    @buffed_stats = Stats.get_blank_stats!

    if not @experience then @experience = 0

    if not @name
      @name = Hero.random_names[math.random(1,#Hero.random_names)]

    -- The direction they are facing
    @facing = direction.down

  serialize: =>
    return encode {@name, @eid, @owner.pid, @x, @y, @facing, @stats\serialize!, @experience}

  -- pid_to_player_fn a function that turns a pid into a Player
  -- so that both client and server can deserialize in their own way.
  deserialize: (serialized, pid_to_player_fn) ->
    decoded = decode serialized
    name = decoded[1]
    eid = tonumber(decoded[2])
    owner = pid_to_player_fn tonumber(decoded[3])
    x,y = tonumber(decoded[4]), tonumber(decoded[5])
    facing = tonumber(decoded[6])
    stats = Stats.deserialize decoded[7]
    experience = tonumber(decoded[8])

    entity = Hero x,y,eid,owner,name,stats,experience
    entity.facing = facing
    return entity

  apply_effect: (cls, level) =>
    -- TODO: check to see if it exists already
    effect = cls level, self
    table.insert @status_effects, effect
    if @effect_window then @effect_window\add_effect effect

  update: (dt) =>
    dead = {}
    for k, effect in pairs @status_effects
      effect\update dt
      if not effect.is_alive
        effect\died!
        table.insert dead, effect
    
    for effect in *dead
      remove_from_array @status_effects, effect
      if @effect_window then @effect_window\remove_effect effect
    
  draw: (image, scroll_x, scroll_y) =>
    -- Get a quad based on what direction we're facing
    quad = love.graphics.newQuad @facing * 49, 0, 48, 50, image\getWidth!, image\getHeight!

    @draw_text_label @name..' ('..tostring(@stats.level)..')', quad, scroll_x, scroll_y

    love.graphics.setColor 255,255,255
    love.graphics.drawq image, quad, @x - scroll_x, @y - scroll_y
    
    -- TODO:
    -- character_graphic should be used for the following (or something extra
    -- like a tint value), but this is just a mock-up
    poisoned = false
    for effect in *@status_effects
      if effect.name == Poison.name
        poisoned = true
        break
    if poisoned
      love.graphics.setColor 0,128,0,150
      love.graphics.drawq image, quad, @x - scroll_x, @y - scroll_y
    
    love.graphics.setColor 255,255,255


mixin_table (getmetatable(Hero)).__index, entity_mixins

-- Unfinished class
class Summon extends Entity
  random_names: {'Summon'}

  new: (@x, @y, @eid, @owner, @name) =>
    super @x, @y, @eid, @owner

    if not @name
      @name = Summon.random_names[math.random(1,#Summon.random_names)]

    -- The direction they are facing
    @facing = direction.down

mixin_table (getmetatable(Summon)).__index, entity_mixins

class Player
  next_pid: 1

  -- name is required
  -- peer is only used by the server
  new:(@name, @pid, @peer) =>
    if not @pid
      @pid = Player.next_pid
      Player.next_pid += 1

    -- Array of entities that this player owns. These are not serialized with the player.
    @entities = {}

    -- Your hero entity is always the first entity that you have
    @hero = nil

  give_entity: (entity) =>
    if #@entities == 0 then @hero = entity
    table.insert @entities, entity
    entity.owner = self

  remove_entity: (entity) =>
    print "[reme] Remove doesn't do anything yet"
    -- Make sure you can't remove your hero

  serialize: =>
    return encode {@name, @pid}

  deserialize: (serialized) ->
    decoded = decode serialized
    new_player= Player decoded[1], tonumber(decoded[2])
    return new_player
