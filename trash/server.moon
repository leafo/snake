require 'enet'
require 'network'
require 'moonscript.util'
require 'shared'
import Color, Entity, Player, Hero from shared

log = (msg) -> print "["..(os.date "%H:%M:%S").."] "..msg

usernames = {"Elrohir", "Feanaro", "Elros"}

-- Forward declarations
handle_message = nil

-- This is a map of event.peer -> Player objects
connected_people = {}

-- Array of all entities
all_entities = {}


num_connected_people = 0 -- todo: turn this into a class

addr_string = "*:"..network.port
log "Starting server on "..addr_string
host = enet.host_create addr_string

broadcast_except_to = (pid, msg) ->
  for peer,player in pairs connected_people
    if player.pid != pid
      peer\send msg

create_new_player = (peer) ->
  name = usernames[num_connected_people]
  if not name then name = "Player "..num_connected_people
  new_player = Player name
  new_player.peer = peer
  connected_people[peer] = new_player

  -- Give that new player a hero
  hero = Hero nil, nil, nil, new_player
  all_entities[hero.eid] = hero

  return new_player

run_server = ->
  while true
    event = host\service 100 -- arg is timeout, in ms
    while event
      if event

        if event.type == "receive" then handle_message event
        elseif event.type == "connect"
          num_connected_people += 1
          connecting_player = create_new_player event.peer
          log connecting_player.name.." connected. There are now "..tostring(num_connected_people).." connected player(s)."

          broadcast_except_to connecting_player.pid, network.form_chat_msg "Server", connecting_player.name.." connected"

          -- Send the connecting player's Player to everyone, including the connecting player
          event.peer\send network.form_your_player_msg connecting_player
          broadcast_except_to connecting_player.pid, network.form_other_player_msg connecting_player

          -- Now that everyone has the Player object, we send out the hero.
          -- All entities need owners, so that's why we needed to send Players first.
          host\broadcast network.form_hero_msg connecting_player.entities[1]


          -- Send the other players' Players and entities to the connecting player
          for peer,player in pairs connected_people
            if player.pid != connecting_player.pid
              event.peer\send network.form_other_player_msg player
              for eid,entity in pairs player.entities
                event.peer\send network.form_hero_msg entity


        elseif event.type == "disconnect"
          disconnecting_player = connected_people[event.peer]
          msg = disconnecting_player.name.." disconnected"
          log msg
          broadcast_except_to disconnecting_player.pid, network.form_disconnect_msg disconnecting_player.pid
          connected_people[event.peer] = nil
          num_connected_people -= 1
      event = host\service 0

handle_message = (event) ->
  player = connected_people[event.peer]

  deserialized = network.deserialize event.data

  msg_type = deserialized[1]

  ---- CHAT MESSAGES ----
  if msg_type == network.type_chat
    chat_msg = deserialized[3]
    log player.name.." typed: "..chat_msg

    -- Prevent someone from making up the "originator" field
    -- by recreating the chat message
    host\broadcast network.form_chat_msg player.name, chat_msg

    if #chat_msg > 0 and (chat_msg\sub 1,1) == "/"
      if #chat_msg > 1 and (chat_msg\sub -1,-1) == "?"
        event.peer\send network.form_chat_msg "Server", "Help commands to come"

      split_msg = moonscript.util.split chat_msg, ' '

  ---- MOVEMENT MESSAGES ----
  elseif msg_type == network.type_move_from_client
    -- TODO: check boundaries

    client_had_correct_coords = (deserialized[2] != player.x or deserialized[3] != player.y)

    if not client_had_correct_coords
      event.peer\send network.form_set_position_msg player.x, player.y

    player.hero\move tonumber(deserialized[4])
    broadcast_except_to player.pid, network.form_server_move_msg player.hero.eid, deserialized[4]

  ---- SPELL MESSAGES ----
  elseif msg_type == network.type_summon
    -- I'm not actually sending Summons yet
    summon = Hero tonumber(deserialized[2]) - 24, tonumber(deserialized[3]) - 25, nil, player
    all_entities[summon.eid] = summon

    host\broadcast network.form_hero_msg summon

  ---- DEBUG MESSAGES ----
  elseif msg_type == network.type_debug_click
    broadcast_except_to player.pid, network.form_click_msg player.pid, deserialized[3], deserialized[4]

  else
    log "Couldn't understand message. Type = "..tostring(msg_type)

run_server!

