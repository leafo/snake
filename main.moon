
require "ui.base"
require "ui.console"
require "graphics.image"

require "network.client"

import Player from require "input.player"

import Window from require "graphics.window"
import Sequence from require "ui.sequence"

unproject = (x, y) ->
  math.floor(x/2), math.floor(y/2)

love.load = ->
  ui.scene = ui.Scene love

  ui_img = image.load"ui.png"
  ui.make_window = ->
    Window ui_img

  console = ui.console.ConsoleWindow 0,0, 300, 80
  love.graphics.print '',0,0 -- print an empty string so that the font will be initialized
  console\initialize!
  ui.scene\push console

  ui.console = console

  seq = Sequence ->
    d = show_dialog 100, 100
    tween d, 0.4, w: 200, h: 75
    d\write_text "hello you are an idiot.... Butts Butts Butts Butts where are the butts"

  p = Player!
  ui.scene\push p
  ui.scene\push p.controller

  ui.scene\push seq

  server = network.client.ServerClient "localhost"
  ui.scene\push server

  love.draw = ->
    love.graphics.scale 2,2
    ui.scene\draw!

