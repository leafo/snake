# Note: handleFile assumes that newlines are a single character!
# Also, if there is whitespace in a file, any newlines will be rewritten as JUST line-feeds ('\n')
import os, time, sys

listArg = False # if true, this will list all lines with whitespace without removing the space
removeArg = False # if true, this will not list any lines with whitespace but will remove the spaces
totalCleanFiles = 0 # The number of files that didn't have any trailing whitespace
numTotalFiles = 0 # number of files handled

disallowed = ('.dll', '.exe')

def handleFile(fname) :
  lowerName = str.lower(fname)
  for d in disallowed :
    if lowerName.endswith(d) : return

  # Don't allow '.git' anywhere in the path
  if lowerName.find('.git') != -1 : return

  try:
    f=open(fname, 'r')
  except IOError:
    print 'Error: can not open text file: ' + str(fname)
    return

  global numTotalFiles
  numTotalFiles += 1
  printedFName = False # only print the filename once, if necessary
  modFileLines = []
  lineNumber = 1
  whitespaceLines = 0
  for line in f :
    hasWhitespace = ( len(line) - len(str.rstrip(line)) > 1 ) # The '1' is the newline at the end of every line
    if hasWhitespace :
      whitespaceLines += 1
      if listArg :
        printStr = str.rstrip(line) # remove the whitespace from the end before printing so that the newline doesn't appear
        if len(printStr) > 40 :
          printStr = "..." + printStr[-40::] # truncate string when printing
        if not printedFName :
          print "File: " + fname
          printedFName = True
        print "\tWhitespace on line #%d: %s" % (lineNumber, printStr)

    if removeArg :
      modFileLines.append(str.rstrip(line) + '\n')

    lineNumber += 1

  f.close()

  if whitespaceLines is 0 :
    global totalCleanFiles
    totalCleanFiles += 1
    return

  if not printedFName :
    print "File: " + fname

  print "There is whitespace on %d line(s)." % (whitespaceLines)
  if len(modFileLines) > 0 :
    try:
      f=open(fname, 'wb') # write binary so that the newlines don't get converted to cr lf
    except IOError:
      print 'Error: can not open text file: ' + str(fname)
      return

    for line in modFileLines :
      f.write(line)

    f.close()

def handleDir(dname) :
  dirList = os.listdir(dname)
  for d in dirList:
    path = os.path.join(dname, d)
    if os.path.isdir(path) == True:
      handleDir(path)
    else:
      handleFile(path)

def main() :
  global listArg
  global removeArg

  if len(sys.argv) < 2 :
    print "Usage: " + sys.argv[0] + " <path> [list | remove] [close]"
    print "List - just list the line numbers with trailing spaces."
    print "Remove - remove all instances of the lines with trailing whitespace"
    print "Close - close after running"
    return

  path = sys.argv[1]

  if len(sys.argv) > 2 :
    thirdArg = str.strip(str.lower(sys.argv[2]))
    if thirdArg == 'list' :
      listArg = True
    if thirdArg == 'remove' :
      removeArg = True
  else :
    # List by default so that you can't screw anything up with drag-and-drop
    listArg = True

  closeAfterFinish = False
  if len(sys.argv) > 3 :
    fourthArg = str.strip(str.lower(sys.argv[3]))
    if fourthArg == 'close' :
      closeAfterFinish = True

  if os.path.isdir(path) :
    handleDir(path)
  else :
    handleFile(path)

  print "\nSummary:\n\nScanned %d files. There were %d files with trailing whitespace." % (numTotalFiles, numTotalFiles - totalCleanFiles)
  # The following is a copy/pastable command to remove the whitespace, but Python doesn't allow copy/paste on Windows. =(
  #~ if not removeArg :
    #~ print sys.argv[0] + " " + sys.argv[1] + " remove"
  if not closeAfterFinish :
    try :
      print "\n(you must manually close this window with ctrl+C, specify 'close' next time if you don't want this)"
      time.sleep(10000)
    except KeyboardInterrupt:
      print "Exited."
      pass

if __name__ == '__main__': main()
