
require "ui.base"
require "support"
module "ui.sequence", package.seeall

-- the methods in here yield to a sequence object

export Sequence, Dialog

interval = (rate, fn) ->
  t = 0
  while true
    done = false
    t += coroutine.yield!
    while t > rate
      t -= rate
      done = fn!
    break if done

-- what to do about extra time?
scope = {
  tween: (time, props) =>
    t = 0 -- need a way to set leftover?
    initial = {}
    for key,_ in pairs props
      initial[key] = self[key]

    while t < 1.0
      for key, finish in pairs props
        self[key] = smoothstep initial[key], finish, t
      t += coroutine.yield! / time

    for key, finish in pairs props
      self[key] = finish

    t - 1.0 -- what to do with this

  show_dialog: (...) ->
    d = Dialog ...
    ui.scene\push d
    d
}

class Dialog
  text_color: {125, 109, 85}
  new: (@x, @y, @w=0, @h=0) ->
    @win = ui.make_window!
    @text = ""

  write_text: (str, speed=0.05) =>
    i = 0
    interval speed, ->
      @text = str\sub 1, i
      i += 1
      true if i > #str

  draw: =>
    @win\draw @x, @y, @w, @h

    if @text
      b = @win.border_size
      text_width = @w - b*2
      love.graphics.setColor unpack @text_color
      love.graphics.printf @text, @x + b, @y + b, text_width
      love.graphics.setColor 255, 255, 255

  remove: =>
    ui.scene\remove self

class Sequence
  new: (fn, _scope=scope) =>
    if _scope
      old_env = getfenv fn
      env = setmetatable {}, {
        __index: (name) =>
          val = scope[name]
          if val
            val
          else
            old_env[name]
      }

      setfenv fn, env

    @fn = coroutine.create fn
    @started = false

  start: =>
    @started = true
    resume @fn

  update: (dt) =>
    @start! if not @started

    if coroutine.status(@fn) == "dead"
      false
    else
      resume @fn, dt
      true

