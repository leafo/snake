
require "support"
require "ui.base"
module "ui.console", package.seeall

export TextLine, ConsoleWindow

find_last_space = (s) ->
  space = string.find s, ' '
  if not space then return nil
  while true
    space2 = string.find s, ' ', space + 1
    if not space2 then return space
    space = space2

class Button
  new: (@x, @y, @width, @height, @func, @r, @g, @b) =>
    @hovering = false
  -- returns true if it did indeed capture this event,
  -- false if it didn't (so that something else could capture it)
  --
  -- x and y are in screen coordinates
  handle_lmb_down: (x, y) =>
    if point_in_rect( x, y, @x, @y, @width, @height )
      @func self
      return true
    return false
  handle_mouse_move: (m_x, m_y) =>
    --~ This is probably masking a bug where hovering isn't set to false all the time because of how events are handled
    if point_in_rect( m_x, m_y, @x, @y, @width, @height )
      @hovering = true
    else
      @hovering = false

  draw: =>
    if @hovering
      love.graphics.setColor @r, @g, @b, 200
    else
      love.graphics.setColor @r, @g, @b, 100

    love.graphics.rectangle "fill", @x, @y, @width, @height
    love.graphics.setColor 255, 255, 255
    love.graphics.rectangle "line", @x, @y, @width, @height

class TextLine
  new: (@text) =>
    @color = Color 255, 255, 255
    if @starts_with "Server: "
      @color.g = 128
      @color.b = 128

  update: =>

  starts_with: (str) =>
    len = #str
    --~ print #@text, len, (@text\sub 1,len), str
    return #@text >= len and (@text\sub 1,len) == str

  draw: (x, y) =>
    love.graphics.setColor @color.r, @color.g, @color.b
    love.graphics.print @text, x, y

class ConsoleWindow
  text_entry_height: 20
  key_chars: {
    ['a']:'A', ['b']:'B', ['c']:'C', ['d']:'D', ['e']:'E', ['f']:'F', ['g']:'G'
    ['h']:'H', ['i']:'I', ['j']:'J', ['k']:'K', ['l']:'L', ['m']:'M', ['n']:'N'
    ['o']:'O', ['p']:'P', ['q']:'Q', ['r']:'R', ['s']:'S', ['t']:'T', ['u']:'U'
    ['v']:'V', ['w']:'W', ['x']:'X', ['y']:'Y', ['z']:'Z'
    ['1']:'!', ['2']:'@', ['3']:'#', ['4']:'$', ['5']:'%', ['6']:'^', ['7']:'&'
    ['8']:'*', ['9']:'(', ['0']:')', [',']:'<', ['.']:'>', ['/']:'?', [';']:':'
    ["'"]:'"', ['[']:'{', [']']:'}', ['-']:'_', ['=']:'+', [' ']:' '
    }

  new: (@x, @y, @width, @height) =>
    @text_lines = { TextLine "Press ~ to close this" } -- remember, in lua, this is really "1:<object>"
    @visible = true
    bwidth, padding = 50, 5

    @rbutton = Button @x + @width - bwidth - padding, @y + padding, bwidth, bwidth, ((b) -> @log "Your hero is at "..tostring(my_player.hero.x)..','..tostring(my_player.hero.y)), 128, 0, 0
    @gbutton = Button @rbutton.x, @y + @rbutton.height + padding, bwidth, bwidth, ((b) -> @log "Clicked green button"), 0, 128, 0
    @type_text = ''
    @blink = 0
    @separator_line_y = @y + @height - @text_entry_height
    
    -- When false, the console only handles 'enter' (and '~' if I haven't disabled that yet)
    @is_typing = false
  -- can't call this until after love was loaded
  initialize: =>
    @font_height = love.graphics.getFont!\getHeight!
    @max_num_text_lines = math.floor( (@height - @text_entry_height) / @font_height)
    @set_cursor_to_end!
  log: (text) =>
    if #@text_lines >= @max_num_text_lines
      @text_lines = [item for item in *@text_lines[2:]]
    @text_lines[#@text_lines + 1] = TextLine text
  update: =>
    @blink += 1
    if @blink == 30 then @blink = 0
    for text_line in *@text_lines
      text_line\update!
  set_cursor_to_end: =>
    @cursor_x = 7 + love.graphics.getFont!\getWidth @type_text

  in_type_mode: =>
    return @visible and @is_typing
  -- Draw the background
  draw_bg: =>
    love.graphics.setColor 0, 0, 0, 100
    love.graphics.rectangle "fill", @x, @y, @width, @height

  draw_fg: =>
    love.graphics.setColor 255, 255, 255
    love.graphics.rectangle "line", @x, @y, @width, @height
  -- see Button.handle_lmb_down
  handle_lmb_down: (x, y) =>
    if @rbutton\handle_lmb_down x, y return true
    if @gbutton\handle_lmb_down x, y return true

    return false

  handle_mouse_move: (m_x, m_y) =>
    if @rbutton\handle_mouse_move m_x, m_y return true
    if @gbutton\handle_mouse_move m_x, m_y return true
    
    return false

  handle_key: (key, unicode) =>
    shift_is_held, ctrl_is_held = ui.query_modifier_keys!

    if key == "`"
      @toggle_vis!
      return true

    if not @visible
      if key == 'return' or key == 'kpenter'
        @toggle_vis!
        @is_typing = true
        return true
      return false

    if key == 'escape' and @is_typing
      @is_typing = false
      @type_text = ''
      return true
  
    if key == 'return' or key == 'kpenter'
      if @is_typing
        if not connected_to_server
          @log @type_text.." (you are not connected to the server)"
        else
          @type_text = moonscript.util.trim(@type_text)
          if string.len(@type_text) > 0
            server\send network.form_chat_msg "unnamed", @type_text
        @type_text = ''
      
      @is_typing = not @is_typing
      
    if not @is_typing then return nil
  
    shift_char = @key_chars[key]

    if key == 'backspace' and #@type_text > 0
      end_index = #@type_text - 1
      if ctrl_is_held
        last_space = find_last_space @type_text
        if last_space == nil
          end_index = 0
        else
          end_index = (find_last_space @type_text)
          if (@type_text\sub end_index, end_index) == ' ' then end_index -= 1

      @type_text = @type_text\sub 1, end_index
      return true

    if shift_char != nil
      if shift_is_held
        @type_text ..= shift_char
      else
        @type_text ..= key

      -- make_part_system @cursor_x, @separator_line_y + @text_entry_height / 2
      return true

    return false

  -- Draw the text entry at the bottom
  draw_text_entry: =>
    -- Draw the line separating it from the output
    love.graphics.setColor 255, 255, 255
    old_line_width = love.graphics.getLineWidth!
    love.graphics.setLineWidth 3
    love.graphics.line @x, @separator_line_y, @x + @width, @separator_line_y
    love.graphics.setLineWidth old_line_width

    -- Draw the text
    love.graphics.print @type_text, @x + 5, @separator_line_y + 4

    -- Draw the cursor
    if @is_typing
      if @blink < 25
        @cursor_x = @x + 7 + love.graphics.getFont!\getWidth @type_text

        love.graphics.line @cursor_x, @separator_line_y + 1, @cursor_x, @y + @height - 1


  draw: =>
    if not @visible then return nil

    @draw_bg!

    index = 0
    for text_line in *@text_lines
      text_line\draw @x, @y + index * @font_height
      index += 1

    @draw_text_entry!

    @rbutton\draw!
    @gbutton\draw!

    @draw_fg!

  toggle_vis: =>
    @visible = not @visible
  show: =>
    @visible = true

