
require "support"
module "ui", package.seeall

export scene, Scene, Button
export query_modifier_keys
scene = nil

responds_to = (method) =>
  type(self[method]) == "function"

query_modifier_keys = =>
  shift_is_held = (love.keyboard.isDown "rshift") or (love.keyboard.isDown "lshift")
  ctrl_is_held = (love.keyboard.isDown "rctrl") or (love.keyboard.isDown "lctrl")
  shift_is_held, ctrl_is_held

class Evented
  new: =>
    @event_table = {}

  register: (name, func) =>
    @event_table[name] = func
  
  register_events: (events) =>
    for name, func in pairs events
      @event_table[name] = func

  dispatch: (name, ...) =>
    handler = @event_table[name]
    handler self, ... if handler

-- self: List
_notify_one = (callback, ...) =>
  for listener in @each!
    fn = listener[callback]
    if fn and fn listener, ...
      return true

  false

class Dispatcher
  -- list name -> func
  lists: {
    drawable: "draw"
    updateable: "update"
    key_listeners: "handle_key"
  }

  new: =>
    for list_name, _ in pairs @lists
      self[list_name] = List!

  push: (item) =>
    for list_name, fn_name in pairs @lists
      if responds_to item, fn_name
        self[list_name]\push_back item

  remove: (item) =>
    for list_name, _ in pairs @lists
      self[list_name]\remove item

  draw: =>
    for item in @drawable\each!
      item\draw!

  update: (dt) =>
    for item in @updateable\each!
      item\update dt

  handle_key: (key, unicode) =>
    _notify_one @key_listeners, "handle_key", key, unicode

  handle_key_up: (key, unicode) =>
    _notify_one @key_listeners, "handle_key_up", key, unicode

class Scene extends Dispatcher
  new: (love) =>
    super!
    @bind love if love

  bind: (love) =>
    scene = self
    love.update = (...) -> @update ...
    love.draw = -> @draw!
    love.keypressed = (...) -> @handle_key ...
    love.keyreleased = (...) -> @handle_key_up ...

  quit: =>
    -- ui.network.disconnect! if ui.network
    os.exit!

  handle_key_up: (key, unicode) =>
    if not super key, unicode
      @quit! if key == "escape"

class Button extends Evented
  new: (events) =>
    super
    @register_events events

  draw: (love) =>
    @dispatch "on_draw"
